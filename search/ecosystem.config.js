module.exports = {
  apps : [{
    name: 'Text Express App',
    script: 'index.js',
    instances: 'max',
    autorestart: true,
    watch: true,
  }],
};

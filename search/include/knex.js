const knex = require('knex')({
  client: 'mysql2',
  connection: {
    host: '127.0.0.1',
    port: 5729,
    user: 'root',
    password: 'root',
    database: 'hn-archive',
    dateStrings: true
  },
});

module.exports = knex;

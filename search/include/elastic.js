const {Client} = require('@elastic/elasticsearch');
const client = new Client({node: 'http://localhost:9200'});

module.exports = {
  search,
  importBulk
};

async function createIndex(name) {
  const response = await client.indices.create({
    index: name,
    body: {
      mappings: {
        properties: {
          text: {type: 'text', analyzer: 'english'},
          content: {type: 'text', analyzer: 'english', term_vector: 'with_positions_offsets'},
        }
      }
    }
  });

  console.log('created index', response);

  return response;
}

async function deleteIndex(name) {
  const response = await client.indices.delete({
    index: name
  });

  console.log('deleted index', response);

  return response;
}

async function importBulk(docs) {
  const bulk_result = await client.helpers.bulk({
    datasource: docs,
    onDocument(doc) {
      return {
        index: {_index: 'hn-archive'}
      };
    },
  });

  console.log('bulk_result', bulk_result);

  return bulk_result;
}

async function search(searchterm, page_size = 20, page_nr = 0) {
  return client.search({
    index: 'hn-archive',
    body: {
      _source: ['id', 'section_id', 'href', 'text'],
      query: {
        multi_match: {
          query: searchterm,
          fields: ['text', 'content'],
          type: 'best_fields',
          operator: 'and'
        }
      },
      size: page_size,
      from: page_nr * page_size,
      highlight: {
        fields: {
          content: {},
        }
      },
      suggest: {
        text: searchterm,
        my_suggest_1: {
          term: {
            field: 'content'
          }
        },
        my_suggest_2: {
          term: {
            field: 'text'
          }
        }
      }
    },
  });
}

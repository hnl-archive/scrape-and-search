const knex = require('./include/knex');
const {importBulk} = require('./include/elastic');

(async function() {
  const docs = await knex('links').where('section_id', '>=', 1821).select();

  await importBulk(docs);

  await knex.destroy();
})();

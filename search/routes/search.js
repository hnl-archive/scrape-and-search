const knex = require('../include/knex');
const {search} = require('../include/elastic');
const logger = require('../include/logger');

module.exports = async function(req, res) {
  const remote_ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();

  if(!req.query.q) {
    logger.error('Error in query, sending 400.', req.query);
    res.status(400).json(false);
    return;
  }

  const page_size = 20;
  const current_page = req.query.p || 0;

  try {
    const search_result = await search(req.query.q, page_size, current_page);
    const result_count = search_result.body.hits.total.value;
    const max_page = Math.floor(search_result.body.hits.total.value / page_size);

    if(current_page > max_page) {
      logger.error('Exceeded max page, sending 404.');
      res.status(404).json(false);
      return;
    }

    let db_response = await knex('sections')
      .whereIn('sections.id', search_result.body.hits.hits.map(hit => hit._source.section_id))
      .join('issues', 'sections.issue_id', 'issues.id')
      .select('sections.id as section_id', 'issue_id', 'title as category', 'date');

    const hits = [];

    for(let hit of search_result.body.hits.hits) {
      hits.push({
        ...db_response.find(result => result.section_id === hit._source.section_id),
        section_id: undefined,
        title: hit._source.text,
        url: hit._source.href,
        text: hit.highlight ? hit.highlight.content.join(' ... ') : ''
      });
    }

    logger.info(`[${remote_ip}] Sending page ${current_page} for query "${req.query.q}", total results: ${search_result.body.hits.total.value}`);

    res.json({
      result_count: result_count,
      max_page: max_page,
      hits: hits
    });

  } catch(e) {
    logger.error('Error in request, sending 503.', e);
    res.status(503).json(false);
  }
};

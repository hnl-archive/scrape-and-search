const knex = require('../include/knex');

module.exports = async function(req, res) {
  let response = await knex('links').count('*', {as: 'count'}).first();
  const total_count = response.count;

  response = await knex('issues').min('id', {as: 'min'}).max('id', {as: 'max'}).first();
  const min_id = response.min;
  const max_id = response.max;

  response = await knex('issues').where('id', min_id).first('date');
  const min_date = response.date;

  response = await knex('issues').where('id', max_id).first('date');
  const max_date = response.date;

  res.json({
    total_count,
    min_id,
    max_id,
    min_date,
    max_date,
  });
};

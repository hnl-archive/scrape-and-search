const knex = require('../include/knex');
const logger = require('../include/logger');

module.exports = async function(req, res) {
  const remote_ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();

  let response = await knex('links')
    .max('id', {as: 'max_id'}).first();

  const random_id =  randomInt(1, response.max_id);

  response = await knex('links')
    .where('id', random_id)
    .first('href');

  logger.info(`[${remote_ip}] Redirecting to random page with id ${random_id}.`);

  res.redirect(response.href);
};

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const express = require('express');
const cors = require('cors');
const app = express();
const initRoute = require('./routes/init');
const searchRoute = require('./routes/search');
const randomRoute = require('./routes/random');

const corsOptions = {
  origin: ['https://hnl-archive.gitlab.io', 'http://localhost:8181'],
  method: ['GET']
};

app.get('/init', cors(corsOptions), initRoute);
app.get('/search', cors(corsOptions), searchRoute);
app.get('/random', cors(corsOptions), randomRoute);

app.listen(3000, () => {
  console.log('Listening on port 3000.');
});


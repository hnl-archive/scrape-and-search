const fs = require('fs');
const path = require('path');
const parseIssue = require('./include/parser');

(async function() {
  const filenames = await fs.promises.readdir('issues_html');
  for(let filename of ['510.html', '511.html', '512.html']) {
    let issue_no = path.parse(filename).name;
    let parsedIssue = parseIssue(issue_no);
    fs.writeFileSync(`issues_json/${issue_no}.json`, JSON.stringify(parsedIssue, null, 2));
  }
})();

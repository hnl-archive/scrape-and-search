const fs = require('fs');
const scrapeContent = require('./include/scraper');
const knex = require('./include/knex');
const logger = require('./include/logger');

(async function() {
  const filenames = await fs.promises.readdir('issues_json');

  for(let filename of ['510.json', '511.json', '512.json']) {
    await jsonToDb(filename);
  }

  await knex.destroy();
})();

async function jsonToDb(filename) {
  logger.debug(`Starting: ${filename}`);

  const json = require(`./issues_json/${filename}`);

  try {
    await knex('issues').insert({
      id: json.issue_no,
      date: json.date,
      quote: json.quote
    });
  } catch(error) {
    logger.error('ERROR inserting issue.', error);
  }

  for(let section of json.sections) {
    logger.debug(`Starting: ${filename}|${section.title}`);

    let section_id;

    try {
      [section_id] = await knex('sections').insert({
        issue_id: json.issue_no,
        title: section.title
      });
    } catch(error) {
      logger.error('ERROR inserting section.', error);
    }

    for(let link of section.links) {
      logger.debug(`Starting: ${filename}|${section.title}|${link.text}`);

      const db_link = {
        section_id: section_id,
        href: link.href,
        text: link.text,
        content: ''
      };

      try {
        const url = link.href.split(/[&?]utm_source/)[0];
        db_link.content = await scrapeContent(url);
      } catch(error) {
        logger.error('ERROR scraping content.', error);
      }

      try {
        await knex('links').insert(db_link);
      } catch(error) {
        logger.error('ERROR inserting link.', error);
      }
    }
  }
}

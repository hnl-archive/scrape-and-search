const winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.printf(({ level, message, ...meta }) => {
      let output = `[${meta.timestamp}] ${level}: ${message}`;
      delete meta.timestamp;
      const meta_string = JSON.stringify(meta);
      if(meta_string !== '{}') {
        output += ` ${meta_string}`;
      }
      return output;
    })
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ level: 'debug', filename: 'logs/debug.log' }),
    new winston.transports.File({ level: 'info', filename: 'logs/info.log' }),
    new winston.transports.File({ level: 'error', filename: 'logs/error.log' }),
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: 'logs/exceptions.log' }),
  ],
  rejectionHandlers: [
    new winston.transports.File({ filename: 'logs/rejections.log' })
  ],
  exitOnError: false
});

module.exports = logger;

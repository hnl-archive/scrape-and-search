const axios = require('axios');
const {JSDOM} = require('jsdom');
const logger = require('./logger');

async function scrapeContent(url) {

  let response;

  try {
    response = await axios.get(url);
  } catch(error) {
    if(!error.response) {
      throw `No response for ${url}`;
    }

    if(error.response.status !== 200) {
      throw `Response status ${error.response.status} for ${url}`;
    }
    throw error;
  }

  if(!response.headers['content-type'] || response.headers['content-type'].substr(0, 4).toLowerCase() !== 'text') {
    throw `Incompatible content-type: ${response.headers['content-type']} for ${url}`;
  }

  let dom;

  try {
    dom = new JSDOM(response.data);
  } catch(error) {
    logger.error('Cannot create JSDOM.', error);
  }

  if(!dom) {
    return '';
  }

  const body = dom.window.document.querySelector('body');

  const tags_to_remove = body.querySelectorAll('script, style');

  for(let tag of tags_to_remove) {
    tag.remove();
  }

  return body.textContent.trim().replace(/[\s\n\r]/gm, ' ').replace(/[\s\n\r]{2,}/gm, ' ');
}

module.exports = scrapeContent;

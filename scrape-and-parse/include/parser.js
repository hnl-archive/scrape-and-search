const fs = require('fs');
const {JSDOM} = require('jsdom');
const chrono = require('chrono-node');

function parseIssue(issue_no) {
  const file = fs.readFileSync(`issues_html/${issue_no}.html`, 'utf8');
  const dom = new JSDOM(file);

  const quote = dom.window.document.querySelector('#header p');

  const content = dom.window.document.querySelector('#content');
  const rows = content.querySelectorAll('td');

  const date = rows[1].textContent.trim().split('//')[1].trim();

  const issue_json = {
    issue_no: issue_no,
    date: chrono.parseDate(date).toISOString().substr(0, 10)
  };

  if(quote && quote.textContent.trim()) {
    issue_json.quote = quote.textContent.trim().replace(/[\s\n\r]{2,}/gm, ' ');
  }

  issue_json.sections = [];

  const link_section = rows[2];

  let current_title;
  let current_section;

  for(let element of link_section.children) {

    switch(element.nodeName.toLowerCase()) {
      case 'h2':
        current_title = element.textContent.trim();

        if(current_section) {
          issue_json.sections.push(current_section);
          current_section = null;
        }

        if(current_title === '#Sponsor') {
          continue;
        }

        current_section = {
          title: current_title,
          links: []
        };

        break;
      case 'p':
        if(!current_section) {
          continue;
        }

        let links = element.querySelectorAll('a');

        for(let link of links) {
          if(!isCommentsLink(link)) {
            current_section.links.push({
              text: link.textContent.trim(),
              href: link.href.trim()
            });
          }
        }
        break;
      default:
        break;
    }
  }

  if(current_section) {
    issue_json.sections.push(current_section);
  }

  return issue_json;
}

function isCommentsLink(link) {
  return !!link.href.match(/news\.ycombinator\.com\/item\?id=[0-9]*&utm_term=comment$/);
}

module.exports = parseIssue;

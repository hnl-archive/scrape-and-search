const axios = require('axios');
const fs = require('fs');

(async function() {
  for(let issue_nr = 510; issue_nr < 513; issue_nr++) {
    let response = await axios.get(`https://mailchi.mp/hackernewsletter/${issue_nr}`);

    if(response.status !== 200) {
      console.log(`Error for issue_nr ${issue_nr}: Return status ${response.status}`);
      continue;
    }

    if(response.data.indexOf('error-page') > -1) {
      console.log(`Found error-page for issue_nr ${issue_nr}`);
      continue;
    }

    fs.writeFileSync(`issues_html/${issue_nr}.html`, response.data);
  }
})();

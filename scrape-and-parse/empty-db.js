const knex = require('./include/knex');

(async function() {
  await emptyDB();
  await knex.destroy();
})();

async function emptyDB() {
  await knex('links').delete();
  await knex('sections').delete();
  await knex('issues').delete();
  await knex.raw('ALTER TABLE `links` AUTO_INCREMENT = 1');
  await knex.raw('ALTER TABLE `sections` AUTO_INCREMENT = 1');
  await knex.raw('ALTER TABLE `issues` AUTO_INCREMENT = 1');
}
